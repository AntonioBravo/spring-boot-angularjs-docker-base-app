(function(angular) {

    var AppController = function($scope, $http, $log) {};

    var CustomDirectivesController = function($scope, $http, $log) {};

    var ProveedorController = function($scope, $http, $log, $rootScope, $location, Proveedor) {

        $scope.registraProveedor = function(proveedor) {
            new Proveedor(proveedor).$save(function(response) {
                $log.info("Registro exitoso!!!");
            })
        }

        $scope.consultaProveedores = function() {
            Proveedor.query(function(response) {
                $log.info("Proveedores --> " + response.length);
                $scope.contadorProveedores = response.length;
                $scope.proveedores = response;
            })
        }

        $scope.consultaProveedor = function(proveedor) {
            $log.info("Proveedor id: " + proveedor.proveedorId);
            Proveedor.get({ proveedorId: proveedor.proveedorId }, function(response) {
                $log.info("Proveedor: " + response.nombre);
                $rootScope.proveedor = response;
                $location.path('/Actualiza');
            })
        }

        $scope.actualizaProveedor = function(proveedor) {
            $log.info("Actualizar proveedor id: " + proveedor.proveedorId);
            Proveedor.update(proveedor.proveedorId, proveedor, function(response) {
                $log.info("Actualización exitosa!!!");
            })
        }

        $scope.eliminaProveedor = function(proveedor) {
            $log.info("Elimina proveedor id: " + proveedor.proveedorId);
            Proveedor.remove({ proveedorId: proveedor.proveedorId }, function() {
                Proveedor.query(function(response) {
                    $log.info("Proveedores --> " + response.length);
                    $scope.contadorProveedores = response.length;
                    $scope.proveedores = response;
                })
            })
        }

    };

    AppController.$inject = ['$scope', '$http', '$log', '$rootScope', '$location', 'Proveedor'];
    angular.module("servicerestApp.controllers").controller("AppController", AppController);
    angular.module("servicerestApp.controllers").controller("CustomDirectivesController", CustomDirectivesController);
    angular.module("servicerestApp.controllers").controller("ProveedorController", ProveedorController);

}(angular));