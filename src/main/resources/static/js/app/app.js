(function(angular) {
    angular.module("servicerestApp.controllers", []);
    angular.module("servicerestApp.services", []);
    angular.module("servicerestApp", ["ui.bootstrap", "ngStorage", "ngResource", "ngRoute", "ngAnimate", "ngTouch", "servicerestApp.controllers", "servicerestApp.services"]);

    angular.module("servicerestApp").config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
            when('/Consulta', {
                templateUrl: '/pages/proveedor/consulta.html',
                controller: 'ProveedorController'
            }).
            when('/Registro', {
                templateUrl: '/pages/proveedor/registro.html',
                controller: 'ProveedorController'
            }).
            when('/Actualiza', {
                templateUrl: '/pages/proveedor/actualiza.html',
                controller: 'ProveedorController'
            }).
            otherwise({
                redirectTo: '/Consulta'
            });
        }
    ]);

    angular.module("servicerestApp").directive("customCollapse", function() {
        return {
            require: '?ngModel',
            scope: {
                ngModel: '='
            },
            restrict: 'A',
            templateUrl: '/accordion/panels.html',
            link: function(scope, el, attrs) {
                    scope.panelBaseId = attrs.collapsePanelBodyId;
                    scope.panelId = attrs.collapsePanelId;

                    $(document).ready(function() {
                        angular.forEach(scope.ngModel, function(value, key) {
                            if (value.collapsed) {
                                $("#" + scope.panelBaseId + "-" + key).collapse("show");
                            }
                        });
                    });

                    scope.toggleCollapsedStates = function(ind) {
                        angular.forEach(scope.ngModel, function(value, key) {
                            if (key == ind) {
                                scope.ngModel[key].collapsed = !scope.ngModel[key].collapsed;
                                $("#" + scope.panelBaseId + "-" + ind).collapse("toggle");
                            } else
                                scope.ngModel[key].collapsed = false;
                        });
                    };

                } //end of link
        };
    });

    var customComponents = angular.module('customComponents', ['servicerestApp']);
}(angular));