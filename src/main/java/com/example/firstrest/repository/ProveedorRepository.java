package com.example.firstrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.firstrest.model.Proveedor;

@Repository("proveedorRepository")
public interface ProveedorRepository  extends JpaRepository<Proveedor, Integer> {

}
