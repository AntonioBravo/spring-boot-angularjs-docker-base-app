## Dockerfile
src/main/docker/Dockerfile

## Genera el jar de la aplicación y crea la imagen de docker con el jar dentro
mvn clean package

## Se crean las siguietes imagenes en docker [docker images]
spring-boot/primerserviciorest   0.0.1-SNAPSHOT
spring-boot/primerserviciorest   latest
frolvlad/alpine-oraclejdk8       slim

## Corre la imagen creada exponiendo el puerto 8085, con el nombre del contenedor "primerserviciorest" y tomamos la última versión instalada "latest"
docker run -d -p 8085:8080 --name primerserviciorest spring-boot/primerserviciorest:latest

## Verificamos que haya levantado correctamente nuestro contenedor [docker ps -a]
CONTAINER ID        IMAGE                                   COMMAND                  CREATED             STATUS                       PORTS                                   NAMES
fe2fe1dcf090        spring-boot/primerserviciorest:latest   "sh -c 'java $JAVA..."   14 seconds ago      Up 14 seconds                0.0.0.0:8085->8080/tcp                  primerserviciorest


## Ejecutamos docker-compose balanceando dos microservicios [docker-compose.yml en raíz]
docker-compose up -d

CONTAINER ID        IMAGE                                   COMMAND                  CREATED             STATUS                           PORTS                                   NAMES
aac3cd0e9c72        dockercloud/haproxy:latest              "/sbin/tini -- doc..."   About an hour ago   Up About an hour                 443/tcp, 0.0.0.0:80->80/tcp, 1936/tcp   springbootangularjsdockerbaseapp_loadbalancer_1
ba9fae7eba1f        spring-boot/primerserviciorest:latest   "sh -c 'java $JAVA..."   About an hour ago   Up About an hour                 8080/tcp                                springbootangularjsdockerbaseapp_microservice_rest_cluster_2_1
75a07b2f6954        dockercloud/haproxy:latest              "/sbin/tini -- doc..."   41 hours ago        Exited (255) 19 hours ago        443/tcp, 0.0.0.0:80->80/tcp, 1936/tcp   complete_loadbalancer_1
